---
title: "[Re] Can gradient clipping mitigate label noise?"

authors:
- David Mizrahi
- admin
- Aiday Marlen Kyzy

date: "2021-01-31T00:00:00Z"
doi: ""
publishDate: "2021-01-01T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]
publication: ReScience C
publication_short: ReScience C

tags: []
featured: false

url_pdf: 'https://openreview.net/forum?id=TM_SgwWJA23'
url_code: 'https://github.com/dmizr/phuber'
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''
---
