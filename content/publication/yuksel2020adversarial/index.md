---
title: "Adversarial Training with Orthogonal Regularization"

authors:
- admin
- İnci Meliha Baytaş

date: "2020-10-05T00:00:00Z"
doi: ""
publishDate: "2021-01-01T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["1"]
publication: 2020 28th Signal Processing and Communications Applications Conference (SIU)
publication_short: SIU

tags: []
featured: false

url_pdf: 'https://ieeexplore.ieee.org/abstract/document/9302247?casa_token=bxalHLXCmJgAAAAA:cmgbDmo7s-QbN9-lWI6HWSYLw_lxWJIRucS53q6s51X_KNL2SeQTcTXkbBjF2OUgwmmvZK_acLo'
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''
---
