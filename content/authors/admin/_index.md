---
# Display name
title: Oğuz Kaan Yüksel

# Is this the primary user of the site?
superuser: true

# Role/position/tagline
# role: Ph.D. student 

# Organizations/Affiliations to show in About widget
organizations:
- name: EPFL
  url: https://www.epfl.ch/en/

# Short bio (displayed in user profile at end of posts)
# bio: A piece of earth that loves to ponder being, life and intelligence.
#My research interests include machine learning, optimization and adversarial robustness.

# Interests to show in About widget
interests:
- Sequential and non-i.i.d. learning
- Self-supervised learning
- Meta-learning

# Education to show in About widget
education:
  courses:
  - course: Ph.D. in EDIC
    institution: École Polytechnique Fédérale de Lausanne
    year: 2022-
  - course: M.Sc. in Data Science & Math (minor)
    institution: École Polytechnique Fédérale de Lausanne
    year: 2020-2022
  - course: B.Sc. in Computer Eng. & Math (double major)
    institution: Boğaziçi University
    year: 2015-2020

# Social/Academic Networking
# For available icons, see: https://wowchemy.com/docs/getting-started/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: mailto:oguz.yuksel_at_epfl.ch
#- icon: twitter
#  icon_pack: fab
#  link: https://twitter.com/okyksl
- icon: google-scholar  # Alternatively, use `google-scholar` icon from `ai` icon pack
  icon_pack: ai
  link: https://scholar.google.com/citations?user=B3ZPLGUAAAAJ&hl=tr
- icon: github
  icon_pack: fab
  link: https://github.com/okyksl/
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.com/okyksl/
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/o%C4%9Fuzkaany%C3%BCksel/
# - icon: cv
#   icon_pack: ai
#   link: uploads/cv.pdf

# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/uploads/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.


# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "okyksl"

# Highlight the author in author lists? (true/false)
highlight_name: true
---

I am a Ph.D. student at [EPFL](https://www.epfl.ch/education/phd/edic-computer-and-communication-sciences/) in the [Theory of Machine Learning Lab](https://www.epfl.ch/labs/tml/), advised by [Nicolas Flammarion](https://people.epfl.ch/nicolas.flammarion).

I focus on statistical learning theory in sequential settings where the traditional i.i.d. assumption does not hold.
My research addresses challenges that arise in scenarios where data points are dependent or sampled from non-stationary distributions, such as in natural language processing or time-series analysis.
By developing theoretical frameworks for these scenarios, I aim to advance our understanding of learning processes in complex, real-world environments.

<!-- Previously, I was an M.Sc. student in Data Science and Mathematics (Minor) at [EPFL](https://www.epfl.ch/en/).
In my thesis, I studied extending random matrix theory results on kernel ridge regression to the ridgeless and negative ridge regimes at the [Chair of Statistical Field Theory](https://www.epfl.ch/labs/csft/) under the supervision of [Berfin Şimşek](https://people.epfl.ch/berfin.simsek) and [Arthur Jacot](https://sites.google.com/view/arthurjacot).
During my studies, I also worked on low-rank matrix completion at the [Chair of Continuous Optimization](https://www.epfl.ch/labs/optim/) under the supervision of [Nicolas Boumal](https://sma.epfl.ch/~nboumal/) and [Quentin Rebjock](https://scholar.google.com/citations?user=t9PEVrkAAAAJ), and on [latent-space perturbations](/publication/yuksel2021semantic-iccv/) at the [Machine Learning and Optimization Lab](https://www.epfl.ch/labs/mlo/), advised by [Tatjana Chavdarova](https://chavdarova.github.io/) and [Sebastian U. Stich](https://www.sstich.ch/).

Before my M.Sc., I finished my Bachelor's degree in Computer Engineering and Mathematics at Boğaziçi University, where I worked on [adversarial robustness and orthogonality in neural networks](https://ieeexplore.ieee.org/abstract/document/9302247), advised by [İnci M. Baytaş](https://cmpe.boun.edu.tr/~incibaytas/), and on normalizing flows and [unsupervised discovery of control for GANs](/publication/yuksel2021latentclr/), advised by [Pınar Yanardağ](https://catlab-team.github.io/). -->

<!-- {{< icon name="download" pack="fas" >}} Download my {{< staticref "uploads/resume.pdf" "newtab" >}}resumé{{< /staticref >}}. -->